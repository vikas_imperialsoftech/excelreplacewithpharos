
import { Component, OnInit } from '@angular/core';
import { stringify } from 'querystring';
import { isNullOrUndefined } from 'util';
import * as XLSX from 'xlsx';
import * as FileSaver from "file-saver";
// import { HttpClient } from '@angular/common/http';
import {HttpClientModule} from '@angular/common/http'
import {HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-excel-operations',
  templateUrl: './excel-operations.component.html',
  styleUrls: ['./excel-operations.component.css']
})
export class ExcelOperationsComponent implements OnInit {

  constructor(private http: HttpClient) { }

  private file : any;
  private arrayBuffer : any;
  private arraylist : any;
  private DataheadersRow : any;
  private OutData :any;
   EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';  
   EXCEL_EXTENSION = '.xlsx';
  ngOnInit(): void {    
  }

  DataFile(event : any){
    try {      
      this.file= event.target.files[0];  
      let fileReader = new FileReader();    
      fileReader.readAsArrayBuffer(this.file);     
      fileReader.onload = (e) => {    
        this.arrayBuffer = fileReader.result;    
        var data = new Uint8Array(this.arrayBuffer);    
        var arr = new Array();    
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);    
        var bstr = arr.join("");    
        var workbook = XLSX.read(bstr, {type:"binary"});    
        var first_sheet_name = workbook.SheetNames[0];    
        var worksheet = workbook.Sheets[first_sheet_name];          
        this.arraylist = XLSX.utils.sheet_to_json(worksheet,{raw:true});    


        // get column data.
        this.http.get('assets/sample-pharos.csv', { responseType: 'blob' }).subscribe(sampleData => {
          const reader: FileReader = new FileReader();
          // reader.readAsBinaryString(data);
    
          console.log('data',sampleData);
          reader.readAsText(sampleData);      
          reader.onload = (e) => {        
            let csvData = reader.result;
            let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);          
            let headersRow = (<string>csvRecordsArray[0]).split(',');  
            this.Opertion(this.arraylist,this.DataheadersRow, headersRow);        
          };
        });
        
      }
    } catch (error) {
      console.log("ExcelOperationsComponent DataFile error : ",error);
    }
  }

  // ColumnFile(event : any){
  //   try{
  //     console.log("ColumnFile Event : ",event);
  //     let files = event.target.files[0];
  //     let reader = new FileReader();
  //     reader.readAsText(files);      
  //     reader.onload = (e) => {        
  //       let csvData = reader.result;
  //       let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);          
  //       let headersRow = (<string>csvRecordsArray[0]).split(',');  
  //       this.Opertion(this.arraylist,this.DataheadersRow, headersRow);        
  //     };      
  //   }
  //   catch(error){
  //     console.log("ExcelOperationsComponent ColumnFile error : ",error);
  //   }
  // }

  Opertion(Data : any, DataHeaders:any, Headers : any){
    try {      
      this.DataheadersRow = [];
      if(!isNullOrUndefined(Data)){
        for(let i=0; i< Data.length; i++){          
          var result = Object.keys(Data[i]);
          for(let j=0; j< result.length; j++)
          {
            let index = this.DataheadersRow.findIndex(x=>x == result[j]);            
            if(index<0){            
              this.DataheadersRow.push(result[j]);                        
            }
          }
        }
      }
      console.log("Data : ",Data);
      console.log("DataheadersRow : ",this.DataheadersRow);      
      console.log("Headers : ",Headers);

      let outa : any = [];
          for(let k=0; k<Data.length;k++)      
          {
            var object = {};          
            for(let i =0 ;i<Headers.length; i++)
            {
              if(Headers[i] == 'VENDOR SKU')
              {
                object[Headers[i]]=Data[k]['SKU']
              }
              else if(Headers[i] == 'UPC')
              {
                object[Headers[i]]=Data[k]['UPC']
              }
              else if(Headers[i] == 'PRODUCT_NAME')
              {
                object[Headers[i]]=Data[k]['PRODUCT_NAME']
              }
              else if(Headers[i] == 'CATEGORY_PATH')
              {
                object[Headers[i]]=Data[k]['CATEGORY_PATH']
              }
              else if(Headers[i] == 'WHOLESALE_PRICE')
              {
                object[Headers[i]]=Data[k]['WHOLESALE_PRICE']
              }
              else if(Headers[i] == 'MinimumQty')
              {
                object[Headers[i]]=Data[k]['MINIMUM']
              }
              else if(Headers[i] == 'UNIT_INCREMENTS')
              {
                object[Headers[i]]=Data[k]['MINIMUM']
              }
              else if(Headers[i] == 'UNIT_OF_MEASUREMENT')
              {
                object[Headers[i]]=Data[k]['UNIT_INCREMENTS']
              }
              else if(Headers[i] == 'UNIT_OF_MEASUREMENT')
              {
                object[Headers[i]]=Data[k]['UNIT_INCREMENTS']
              }
              else if(Headers[i] == 'MSRP')
              {
                object[Headers[i]]=Data[k]['MSRP']
              }
              else if(Headers[i] == 'AVAILABILITY_MESSAGE')
              {
                object[Headers[i]]=Data[k]['AVAILABILITY_MESSAGE']
              }
              else if(Headers[i] == 'SHIP_ATTR_1')
              {
                object[Headers[i]]='Meri Meri'
              }
              else if(Headers[i] == 'PHAROS_VENDOR_ID')
              {
                object[Headers[i]]='406712'
              }
              else if(Headers[i] == 'SKU')
              {
                object[Headers[i]]='MERI'+Data[k]['SKU']
              }
              else
              {
                object[Headers[i]] = ''
              }
          } 
          if(object!={})
          {              
            outa.push(object);
          }
      }      
      this.OutData = outa;
    } catch (error) {
      console.log("ExcelOperationsComponent Opertion error : ",error);
    }
  }

  onExcelExport(): void {
    try {  
      const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.OutData);  
      const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };  
      const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });  
      this.saveAsExcelFile(excelBuffer, "OutputExcel"); 
    } catch (error) {
      console.log("ExcelOperationsComponent onExcelExport error : ",error);
    }
  }

  private saveAsExcelFile(buffer: any, fileName: string): void 
  {  
    const data: Blob = new Blob([buffer], {type: this.EXCEL_TYPE});  
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + this.EXCEL_EXTENSION);  
  }   

}
